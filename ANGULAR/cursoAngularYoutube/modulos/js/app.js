// Un módulo es una forma de organizar y agrupar funcionalidad, esto con la idea de poder distribuirlo 
// fácilmente así como poder crear pruebas automatizadas de manera modular. 
// También podemos decir que es un contenedor donde almacenamos nuestros controllers, directivas, filtros, etc.
(function() {
    "use strict";
    var app = angular.module('nameApp', []);

    console.log(app)
})();